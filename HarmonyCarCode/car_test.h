/*
 * Copyright (c) 2020 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CAR_TEST_H__
#define __CAR_TEST_H__

#define CAR_STEP_COUNT_GO      150
#define CAR_STEP_COUNT_RIGHT_LEFT      75

typedef enum {
    /*停止*/
    CAR_STATUS_STOP,

    /*前进*/
    CAR_STATUS_FORWARD,

    /*后退*/
    CAR_STATUS_BACKWARD,

    /*左转*/
    CAR_STATUS_LEFT,

    /*右转*/
    CAR_STATUS_RIGHT,

    /** Maximum value */
    CAR_STATUS_MAX
} CarStatus;


typedef enum {
    /*收到前进、后退指令后，只会走一小段距离，然后停止*/
    CAR_MODE_STEP,

    /*收到前进、后退指令后，会一直走*/
    CAR_MODE_ALWAY,

    /** Maximum value */
    CAR_MODE_MAX
} CarMode;


struct car_sys_info {

    CarStatus cur_status;
    CarStatus go_status;
    char status_change;

    CarMode mode;

    int step_count;
} ;


void car_test(void);

void set_car_status(CarStatus status);

void set_car_mode(CarMode mode);

#endif /* __CAR_TEST_H__ */

